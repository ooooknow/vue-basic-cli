module.exports = {
    root: true,
    env: {
        browser: true,
        node: true,
        es6: true,
    },
    extends: ['plugin:vue/essential', '@vue/airbnb'],
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'import/no-cycle': 'off',
        'max-len': ['warn', { code: 600 }],
        indent: [
            'warn',
            4,
            {
                SwitchCase: 1,
            },
        ],
    },
    parserOptions: {
        parser: 'babel-eslint',
        sourceType: 'module',
    },
};
