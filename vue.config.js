// 代理请求地址
const APIURL = 'http://www.test-api.com/';
const autoprefixer = require('autoprefixer');
const mockServer = require('./mock/mock-server.js');

module.exports = {
    outputDir: 'dist',
    indexPath: 'index.html',
    publicPath: process.env.NODE_ENV === 'production' ? './' : '/',
    productionSourceMap: false,
    css: {
        loaderOptions: {
            postcss: {
                plugins: [autoprefixer],
            },
        },
    },
    devServer: {
        port: '9527',
        open: true,
        overlay: {
            warnings: false,
            errors: true,
        },
        proxy: {
            'dev-api': {
                target: `http://127.0.0.1:${9527}/mock`,
                changeOrigin: true,
                pathRewrite: {
                    '^dev-api': '',
                },
            },
            '/api': {
                target: APIURL,
                changeOrigin: true,
                ws: true,
                pathRewrite: {
                    '^/api': '/api',
                },
            },
        },
        after: mockServer,
    },
    lintOnSave: undefined,
};
