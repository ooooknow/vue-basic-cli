import Vue from 'vue';
import _ from 'lodash';
import App from '@/App.vue';
import router from '@/router';
import store from '@/store';
import '@/plugins/registerServiceWorker';
import '@/plugins/element';
import '@/styles/index.scss';
import '@/utils/permission';

Vue.config.productionTip = false;
Object.defineProperty(Vue.prototype, '_', { value: _ });

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app');
