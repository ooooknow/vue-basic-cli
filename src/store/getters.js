const getters = {
    sidebar: state => state.app.sidebar,
    device: state => state.app.device,
    role: state => state.user.role,
    user: state => state.user.user,
    token: state => state.user.token,
};
export default getters;
