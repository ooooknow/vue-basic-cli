import cookies from 'js-cookie';
import { login, getUserInfo, logout } from '@/api/user';

const user = {
    namespaced: true,
    state: {
        // 用户权限
        role: cookies.get('role') || [],
        // 请求头
        token: cookies.get('token') || '',
        // 用户数据
        user: {},
    },
    mutations: {
        SET_ROLE(state, role) {
            state.role = role;
        },
        SET_USER(state, obj) {
            state.user = obj;
        },
        SET_TOKEN(state, token) {
            state.token = token;
        },
    },
    actions: {
        // 登录
        AdminLogin({ commit }, loginForm) {
            return new Promise((resolve, reject) => {
                login(loginForm)
                    .then((res) => {
                        if (res) {
                            commit('SET_TOKEN', res.token);
                            commit('SET_USER', res.user);
                            cookies.set('token', res.token);
                            const role = res.user.role.join(',');
                            commit('SET_ROLE', role);
                            cookies.set('role', role);
                            resolve();
                        } else {
                            reject();
                        }
                    })
                    .catch((error) => {
                        reject(error);
                    });
            });
        },
        // 获取用户信息
        AdminGetInfo({ commit }) {
            return new Promise((resolve, reject) => {
                getUserInfo()
                    .then((res) => {
                        commit('SET_USER', res.user);
                        resolve();
                    })
                    .catch((error) => {
                        reject(error);
                    });
            });
        },
        // 登出
        AdminLogout({ commit }) {
            return new Promise((resolve, reject) => {
                logout()
                    .then(() => {
                        commit('SET_ROLE', []);
                        commit('SET_TOKEN', '');
                        cookies.remove('role');
                        cookies.remove('token');
                        resolve();
                    })
                    .catch((error) => {
                        reject(error);
                    });
            });
        },
        // 网络出错情况下清除token
        ResetToken({ commit }) {
            return new Promise((resolve) => {
                commit('SET_ROLE', []);
                commit('SET_TOKEN', '');
                cookies.remove('role');
                cookies.remove('token');
                resolve();
            });
        },
    },
};
export default user;
