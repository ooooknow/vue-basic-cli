/* eslint-disable no-plusplus */
import store from '@/store';


// 路径检测
export function isExternal(path) {
    return /^(https?:|mailto:|tel:)/.test(path);
}


// 设置页面标题
export function getPageTitle(pageTitle) {
    if (pageTitle) {
        return `${pageTitle} - test`;
    }
    return 'test';
}

export function param2Obj(url) {
    const search = url.split('?')[1];
    if (!search) {
        return {};
    }
    return JSON.parse(
        `{"${
            decodeURIComponent(search)
                .replace(/"/g, '\\"')
                .replace(/&/g, '","')
                .replace(/=/g, '":"')
                .replace(/\+/g, ' ')
        }"}`,
    );
}

export function deepClone(source) {
    if (!source && typeof source !== 'object') {
        throw new Error('error arguments', 'deepClone');
    }
    const targetObj = source.constructor === Array ? [] : {};
    Object.keys(source)
        .forEach((keys) => {
            if (source[keys] && typeof source[keys] === 'object') {
                targetObj[keys] = deepClone(source[keys]);
            } else {
                targetObj[keys] = source[keys];
            }
        });
    return targetObj;
}

// 权限判断
export function roleJudge(toRole) {
    if (store.getters.role.split) {
        const userRole = store.getters.role;
        const some = toRole.some(item => item === userRole);
        console.log(toRole, some);
        return some;
    }
    return false;
}
