import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import { Message } from 'element-ui';
import router from '@/router';
import store from '@/store';
import { getPageTitle, roleJudge } from '@/utils';

NProgress.configure({ showSpinner: false });


// 忽略路由
const whiteList = ['/login', '/404'];

router.beforeEach(async (to, from, next) => {
    NProgress.start();

    document.title = getPageTitle(to.meta.title);

    const hasToken = store.getters.token;
    if (hasToken) {
        if (to.path === '/login') {
            next({ path: '/' });
            NProgress.done();
        } else if (store.getters.username) {
            if (to.meta.roles) {
                if (!roleJudge(to.meta.roles)) {
                    Message.error('抱歉，您无权限访问');
                    next({ path: '/' });
                    NProgress.done();
                } else {
                    next();
                }
            } else {
                next();
            }
        } else {
            try {
                await store.dispatch('user/AdminGetInfo');
                next();
            } catch (error) {
                await store.dispatch('user/ResetToken');
                next(`/login?redirect=${to.path}`);
                NProgress.done();
            }
        }
    } else if (whiteList.indexOf(to.path) !== -1) {
        next();
    } else {
        next(`/login?redirect=${to.path}`);
        NProgress.done();
    }
});

router.afterEach(() => {
    NProgress.done();
});
