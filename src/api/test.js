// eslint-disable-next-line import/no-cycle
import axios from '@/plugins/axios';

// 测试列表
export function getTestList() {
    return axios({
        url: 'test/list',
        method: 'get',
    });
}

// 测试详情
export function getTestInfo(id) {
    return axios({
        url: `test/info/${id}`,
        method: 'get',
    });
}
