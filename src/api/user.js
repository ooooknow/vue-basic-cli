import qs from 'qs';
import axios from '@/plugins/axios';
// 登录
export function login(loginFrom) {
    return axios({
        url: 'login',
        method: 'post',
        data: qs.stringify(loginFrom),
    });
}

// 登出
export function getUserInfo() {
    return axios({
        url: 'user-info',
        method: 'get',
    });
}
// 登出
export function logout() {
    return axios({
        url: 'logout',
        method: 'post',
    });
}
