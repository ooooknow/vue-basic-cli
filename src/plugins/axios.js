import axios from 'axios';
import promise from 'es6-promise';
import { Message } from 'element-ui';
// eslint-disable-next-line import/no-cycle
import store from '@/store';
import router from '@/router';

promise.polyfill();

const service = axios.create({
    baseURL: 'dev-api/',
});

// 请求拦截器
service.interceptors.request.use(
    config => config,
    (error) => {
        console.log(error);
        Promise.reject(error);
    },
);

// 响应拦截
service.interceptors.response.use(
    async response => response,
    (error) => {
        switch (error.response.status) {
            // 401: 未登录
            case 401:
                Message.error('登录状态过期请重新登录');
                store.commit('SET_USER', '{}');
                store.commit('SET_HEADERS', '{}');
                router.replace({
                    path: '/login',
                });
                break;
            // 403 token过期
            case 403:
                Message.error('权限不足');
                break;
            // 404请求不存在
            case 404:
                Message.error('网络请求出错，请重新尝试');
                break;
            // 其他错误
            default:
                Message.error('网络请求出错，请重新尝试');
        }
        return Promise.reject(error.response);
    },
);

export default service;
