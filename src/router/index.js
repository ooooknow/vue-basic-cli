import Vue from 'vue';
import Router from 'vue-router';
import Layout from '../layout/index.vue';
import ParentRouter from '@/layout/ParentRouter.vue';
import Dashboard from '@/views/Dashboard/index.vue';

Vue.use(Router);
// 重写go方法
Router.prototype.go = () => {
    this.isBack = true;
    window.history.go(-1);
};

const constantRouterMap = [
    {
        path: '/login',
        component: () => import('@/views/Login'),
        hidden: true,
    },
    {
        path: '/404',
        component: () => import('@/views/404'),
        hidden: true,
    },
    {
        path: '/',
        component: Layout,
        redirect: '/dashboard',
        hidden: true,
        children: [{
            path: 'dashboard',
            name: 'Dashboard',
            component: () => Dashboard,
            meta: {
                title: '首页',
                roles: ['admin'],
                icon: 'icon-liebiao1',
            },
        }],
    },
    {
        path: '/test',
        name: 'Test',
        component: Layout,
        redirect: '/test/parent/list',
        meta: {
            title: '测试一级标题',
            icon: 'icon-liebiao1',
            roles: ['admin'],
        },
        children: [
            {
                path: 'parent',
                name: 'TestParent',
                component: ParentRouter,
                redirect: '/test/parent/list',
                meta: {
                    title: '测试二级标题',
                    icon: 'icon-liebiao1',
                    roles: ['admin'],
                },
                children: [
                    {
                        path: 'list',
                        name: 'TestList',
                        component: () => import('@/views/Test/List'),
                        meta: {
                            title: '测试列表',
                            icon: 'icon-liebiao1',
                            roles: ['admin'],
                        },
                    },
                    {
                        path: 'info',
                        name: 'TestInfo',
                        component: () => import('@/views/Test/Info'),
                        hidden: true,
                        meta: {
                            title: '测试详情',
                            icon: 'icon-liebiao1',
                            roles: ['admin'],
                        },
                    },
                ],
            },
        ],
    },
];

export default new Router({
    base: process.env.BASE_URL,
    routes: constantRouterMap,
});
